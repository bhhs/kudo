#Concept & Topic

Our goal was to create a user friendly message sharing app for our school. It had to be simple yet 
aesthetically pleasing. Future android apps are trending toward less cold, clean,
layouts and more colorful, friendly designs.

An app is only useful if it's accepted by its target audience. For this reason, we created an 
interface that was fun and colorful, beyond a boring, predictable messaging format. Thus, Kudo
was born: a cheerful parrot living in a rainbow jungle of information. Kudo is a derivative of 
kudos meaning praise and achievement. His thumbs up stance exemplifies a positive, "way to go"
attitude. Of course, a parrot is known for repeating messages: the purpose of our app.

The tagline, "Gotta know? Kudo!" is a catchy, modern call to action especially appealing to students.

This app fulfills several main functions. You can retrieve messages, create your own group, and 
share events to social media.

#Graphics

Android's Holo Dark was used as our parent theme. A chalkboard window background was
appropriate to "scratch out" school announcements. The rainbow colored, tropical details were 
researched to be the most eye appealing to the student demographic. 

A grey font against the dark theme enhances comfortable readability. The call to action buttons 
are optimally placed and visible.

All images were resized to support multiple screen densities.

Photos were taken from a variety of sources, all are legal to be used for commercial purposes.

Icon            |   Source
----------------|-----------------
ic\_launcher     |   Purchased from http://www.canstockphoto.com/images-photos/parrot.html#file_view.php?id=8846585 
background      | http://pixabay.com/en/blackboard-chalkboard-background-569260/
ab_background   | http://pixabay.com/en/abstract-background-texture-color-268834/
ic\_action\_group | Android SDK
ic\_action\_new   | Android SDK
ic\_action\_overflow  |   Android SDK
ic\_action\_share | Android SDK
ic\_action\_watch | https://openclipart.org/detail/188640/1384151134-by-SumiTomohiko
image\_birds\_with\_sign   | Purchased from http://www.canstockphoto.com/macaw-bird-with-blank-signboard-10327442.html
image\_feather   |   http://pixabay.com/en/feather-ink-write-pen-retro-old-28021/

*[More information](https://developer.android.com/design/downloads/index.html) about the Android SDK images.

#Social Media Elements

A share button on the action bar allows the user to easily post to Facebook, Google Plus, 
Twitter, Email, and others.

![Share Screenshot](http://kudo.gocybered.com/images/screenshot_share.png)

#Icons
![launcher](http://kudo.gocybered.com/images/ic_launcher.png)
Our launcher icon is Kudo the Parrot, specifically chosen for his ability
to encourage students to, "parrot back information." He's proudly wearing 
Big Horn school colors. His big thumbs up empowers students through a 
recognizable gesture of positive approval and praise. Kudos!

The icon buttons on the taskbar are as follows depending on context:

 - ![Share](http://kudo.gocybered.com/images/ic_action_share.png) for connecting to social media
 - ![Track](http://kudo.gocybered.com/images/ic_action_watch.png) Track code - Input screen for adding a code to your feed
 - ![Account](http://kudo.gocybered.com/images/ic_action_group.png) Account access including sign in, creating an account, and managing codes once generated
 - ![New](http://kudo.gocybered.com/images/ic_action_new.png) Generates a new code for your organization
 - ![Home](http://kudo.gocybered.com/images/ic_action_home.png) Returns the user to home feed from a management activity
 - ![Add event to calendar](http://kudo.gocybered.com/images/ic_action_calendar.png) Adds an event directly to the default Android calendar.
 - ![Overflow](http://kudo.gocybered.com/images/ic_action_overflow.png) Shows hidden action bar items
 - FAQs - Where the user can find more in depth information

#Sample Use
_Scenerio: You're at a pep assembly supporting your school, Big Horn High School. The student 
body president announces their upcoming activities can be found on Kudo with the code **AAAA.**
You just downloaded the app, now what?_

![First Run](http://kudo.gocybered.com/images/screenshot_first_run.png)

When first running the app, this screen welcomes you. To begin tracking Big Horn's announcements, 
first press the ![eye](http://kudo.gocybered.com/images/ic_action_watch.png) icon at the top right
of the action bar. This will initiate the track code activity.

![Track Code](http://kudo.gocybered.com/images/screenshot_track_code.png)

On the track code page, simply enter the code for Big Horn (AAAA) and press go.

Confirm you're about to follow the next code and tap __Track Code__.

![Feed Activity](http://kudo.gocybered.com/images/screenshot_feed.png)

Now that you're tracking Big Horn, you will see a list of all their upcoming activities and events.
Just tap an entry for more details.

![Detail View](http://kudo.gocybered.com/images/screenshot_details.png)

Detailed information about the event is displayed. This includes a title, description, start and end times, and contact information, if available. Tap ![Calendar](http://kudo.gocybered.com/images/ic_action_calendar.png) to add the event directly to the default android Calendar. This is only supported on devices running Ice Cream Sandwich and higher, otherwise the button will be hidden.

![Detail View](http://kudo.gocybered.com/images/screenshot_calendar_intent.png)

Tap save to immediately add the event to your calendar.

Inspired, you decide to create your own code for your FBLA chapter. First, tap the ![account](http://kudo.gocybered.com/images/ic_action_group.png) button.

![Sign In](http://kudo.gocybered.com/images/screenshot_sign_in.png)

Immediately, you are presented with a login screen. Either type an existing username & password 
and tap _Sign In_ or simply _Create Account_. Because an account is not needed to track codes, most 
likely you will need to create a new account to become a manager.

![No Managed Feeds](http://kudo.gocybered.com/images/screenshot_no_managed_feeds.png)

After signing in, you arrive at the management section. Generate a new code (feed) 
by tapping ![the plus](http://kudo.gocybered.com/images/ic_action_new.png).

![Create code title/description](http://kudo.gocybered.com/images/screenshot_generate_code_input.png)

Type the title for your new code (Big Horn FBLA) and a description. The description can span multiple
lines. When finished, tap _Generate Code_.

![Share and Create your first announcement](http://kudo.gocybered.com/images/screenshot_generate_code_output.png)

Your unique code will be displayed. In this case, the code is WYAD. From here you may share
the code (via ![the share button](http://kudo.gocybered.com/images/ic_action_share.png)) with 
fellow FBLA members, create your first announcement, or both!

#Security
Several additional precautions were taken to ensure using this app is secure. All communication between the app and Kudo server is encrypted with **TLS**. This is accomplished using a self-signed certificate authority to create public/private keys for the Kudo server. Although the certificate is self-signed (using a 2048 bit modulus), it is still fully validated using the app. The app includes a copy the public CA key (named caroot.pem in the raw resource directory) which is loaded into a custom keystore (see the SslHelper class). 

In addition, a **token system** has been implemented on the server. Any actions that require an account (generating new codes, creating announcements, etc) send a one-time-use token to the server to validate the user's identity. If a proper token is sent to the server, the server will respond with the requested data as a JSON object. The JSON object will include another token, which is saved and used for the next transaction. This **mantains strong security** within the app **without** forcing the user to login each and every time they perform a management function.

#Developer Notes
Kudo was developed with **Android Studio** version 1.1.0 and Gradle version 2.2.1. 

###Getting the Source Code
The easiest way to get the source code is by cloning the git repository that can
be found at: https://bitbucket.org/bhhs/kudo/overview

    git clone https://bitbucket.org/bhhs/kudo.git
    
This will download both the Android source for the application, as well as the PHP code that runs
the back-end server.

###Importing the Project
Once the source code has been downloaded, it will need to be imported into an IDE. Because Kudo was 
built with [Android Studio](http://developer.android.com/sdk/index.html), it's best to stick to that.

![Android Studio Splash Screen](http://kudo.gocybered.com/images/screenshot_open_existing.png)

When Android Studio launches, choose _Open an existing Android Studio project_

![Android Studio import build.gradle](http://kudo.gocybered.com/images/screenshot_import_build_gradle.png)

Choose to import _build.gradle_ under the android app folder. Do **not** import the parent Kudo 
folder -- the PHP code under the web directory will cause problems.

![Accept Android Studio's defaults](http://kudo.gocybered.com/images/screenshot_import_defaults.png)

Check that your settings match those above and press _OK_. Gradle will automatically be downloaded 
if needed and the project will open.

That's it. You're ready to start contributing to the project. Now, go share some Kudos!