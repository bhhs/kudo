<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 2/28/15
 * Time: 9:17 PM
 */

require "DbHelper.php";

$username = $_POST['user'];
$token = $_POST['token'];
$code = $_POST['code'];
$title = $_POST['title'];
$description = $_POST['description'];
$startEpoch = $_POST['start'];
$endEpoch = $_POST['end'];

$db = new DbHelper();

$newToken = $db->verifyToken($username,$token);
if($newToken != ""){
    $announcement = new Announcement($db->usernameToUserId($username),$code,$title);
    $announcement->setEffective($startEpoch);
    $announcement->setExpiry($endEpoch);
    $announcement->setContent($description);

    $db->insertAnnouncement($announcement);

    echo "[{\"token\":\"$newToken\"}]";
}