<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 2/22/15
 * Time: 1:21 AM
 */

require "DbHelper.php";

$username = $_POST['user'];
$password = $_POST['password'];
$first = $_POST['first'];
$last = $_POST['last'];
$email = $_POST['email'];

if($username != "" && $password != "" && $first != "" && $last != "" && $email != ""){
    $db = new DbHelper();

    //generate password hash
    $password = CryptHelper::generate($password);

    //add user to db
    $user = new User($username, $password, $first, $last, $email);
    $db->insertUser($user);

    //get token
    $newToken =  $db->registerToken($username);

    echo "[{\"token\":\"$newToken\"}]";
}