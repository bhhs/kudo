<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 12/22/14
 * Time: 6:56 PM
 */

class Announcement implements JsonSerializable{
    private $id = null;             //Announcement ID, should only be set from SQL AUTO_INCREMENT value
    private $userId = null;         //userId who created announcement
    private $code = null;           //Users watching this code will receive the announcement
    private $title = null;          //Title of the announcement
    private $content = "";          //optional - The announcement's body
    private $effective = null;      //the date the announcement's start time (Epoch)
    private $expiry = null;         //the end date of the announcement (Epoch)
    private $photoURL = "";         //optional - the URL of the photo for the CODE -- kept in announcement only for compatibility. Will be removed in future!

    public function __construct($userId, $code, $title){
        $this->setUserId($userId);
        $this->setCode($code);
        $this->setTitle($title);
    }

    public function __toString(){
        $str = <<<EOT
        <pre>
        id: $this->id
        userId: $this->userId
        code: $this->code
        content: $this->content
        effective: $this->effective
        expiry: $this->expiry
        photoURL: $this->photoURL
        </pre>
EOT;
        return $str;
    }

    public function jsonSerialize(){
        $result = get_object_vars($this);
        return json_encode($result);
    }

    //setters
    public function setId($id){
        $this->id = $id;
    }
    public function setUserId($userId){
        $this->userId = $userId;
    }
    public function setCode($code){
        $this->code = $code;
    }
    public function setTitle($title){
        $this->title = $title;
    }
    public function setContent($content){
        $this->content = $content;
    }
    public function setEffective($effective){
        $this->effective = $effective;
    }
    public function setExpiry($expiry){
        $this->expiry = $expiry;
    }
    public function setPhotoURL($photoURL){
        $this->photoURL = $photoURL;
    }

    //getters
    public function getId(){
        return $this->id;
    }
    public function getUserId(){
        return $this->userId;
    }
    public function getCode(){
        return $this->code;
    }
    public function getTitle(){
        return $this->title;
    }
    public function getContent(){
        return $this->content;
    }
    public function getEffective(){
        //default to current time
        if($this->effective == null){
            $this->effective = time();
        }

        return $this->effective;
    }
    public function getExpiry(){
        return $this->expiry;
    }
    public function getPhotoURL(){
        return $this->photoURL;
    }
}