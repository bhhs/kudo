<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 2/28/15
 * Time: 2:45 AM
 */

require "DbHelper.php";

$username = $_POST['user'];
$token = $_POST['token'];

$db = new DbHelper();
$newToken = $db->verifyToken($username,$token);

if($newToken == null || $newToken == ""){
    die("Invalid token");
}

$user = $db->selectUserSudo($user);

echo "[";
$i = 0;
echo "{\"token\":\"$newToken\"},";
echo $user->jsonSerialize();
echo "]";
