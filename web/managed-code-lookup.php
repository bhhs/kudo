<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 2/27/15
 * Time: 10:59 PM
 */

require "DbHelper.php";

$user = $_POST['user'];
$token = $_POST['token'];

$db = new DbHelper();
$newToken = $db->verifyToken($user,$token);

if($newToken == null || $newToken == ""){
    die("Bad token");
}

//convert username to ID
$userId = $db->usernameToUserId($user);

//get list of all managed codes
$codeList = $db->selectManagedCodes($userId);

echo "[";
$i = 0;
echo "{\"token\":\"$newToken\"}";
while($i < count($codeList) ){
    //because token starts array, always append a comma
    echo ",";
    echo $codeList[$i];
    $i++;
}
echo "]";