<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 1/31/15
 * Time: 8:52 PM
 */

require "DbHelper.php";

$codesCSV = $_GET['code'];
$codes = array_map("str_getcsv", preg_split('/\r*\n+|\r+/', $codesCSV));
$codes = $codes[0];

$db = new DbHelper();
$announceList = $db->selectAnnouncements($codes);

echo "[";
$i = 0;
while($i < count($announceList) ){
    if($i != 0) {
        echo ",";
    }
    echo $announceList[$i];
    $i++;
}
echo "]";