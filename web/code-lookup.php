<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 12/18/14
 * Time: 10:01 PM
 */

require "DbHelper.php";

$code = $_GET['code'];

$db = new DbHelper();
$code = $db->selectOneCode($code);

echo $code->jsonSerialize();