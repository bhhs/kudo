<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 2/26/15
 * Time: 7:37 PM
 */

class TokenGenerator {
    static public function generate(){
        $token = bin2hex(openssl_random_pseudo_bytes(16));

        return $token;
    }
}