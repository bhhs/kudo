<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 12/18/14
 * Time: 10:50 PM
 */

class Code implements JsonSerializable{
    private $id = null;             //Unique ID automatically assigned in SQL
    private $code = null;           //The code's unique value the user enters to receive updates - UPPERCASE
    private $userId = null;         //The userId of the person who created the code
    private $name = null;           //The name of the code (eg, "Big Horn Announcements")
    private $description = null;    //A longer description (eg, "A small high school in Wyoming...") - optional
    private $photo = null;          //Photo to use for all announcements under this code

    public function __construct($code,$userId,$name,$description){
        $this->setCode($code);
        $this->setUserId($userId);
        $this->setName($name);
        $this->setDescription($description);
    }

    public function __toString(){
        $str = <<<EOT
        <pre>
        id: $this->id
        code: $this->code
        userId: $this->userId
        name: $this->name
        description: $this->description
        </pre>
EOT;
        return $str;
    }

    public function jsonSerialize(){
        $result = get_object_vars($this);
        return json_encode($result);
    }

    //getters
    public function getId(){
        return $this->id;
    }
    public function getCode(){
        return $this->code;
    }
    public function getUserId(){
        return $this->userId;
    }
    public function getName(){
        return $this->name;
    }
    public function getDescription(){
        return $this->description;
    }
    public function getPhoto(){
        return $this->photo;
    }

    //setters
    public function setId($id){
        //this method should ONLY be called when populating code obj from SQL DB
        $this->id = $id;
    }
    public function setCode($code){
        $this->code = strtoupper($code);
    }
    public function setName($name){
        $this->name = $name;
    }
    public function setUserId($userId){
        $this->userId = $userId;
    }
    public function setDescription($desc){
        $this->description = $desc;
    }
    public function setPhoto($photo){
        $this->photo = $photo;
    }

}