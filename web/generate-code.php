<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 2/22/15
 * Time: 12:59 AM
 */

require "DbHelper.php";

$db = new DbHelper();

$username = $_POST['user'];
$token = $_POST['token'];

$name = $_POST['name'];
$description = $_POST['description'];
$photo = $_POST['photo'];

$userId = $db->usernameToUserId($username);

if($userId != "" && $name != "" && $description != "") {
    $newToken = $db->verifyToken($username, $token);
    if ($newToken != null) {
        $code = $db->generateCode($userId, $name, $description, $photo);
        echo "[";
        echo "{\"token\":\"$newToken\"},";
        echo $code->jsonSerialize();
        echo "]";
    }
}