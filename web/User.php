<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 12/22/14
 * Time: 11:16 PM
 */

class User implements JsonSerializable{
    private $userId = null;         //Unique 'Id', should only be set from SQL query value
    private $username = null;       //Username - required
    private $password = null;       //encrypted password - required
    private $firstName = null;      //User's first name - required
    private $lastName = null;       //User's last name - required
    private $email = null;          //User's email - required
    private $memberSince = null;    //Date user was created, this is NOT autocreated by DB. Returns 'YYYY-MM-DD' - required

    public function __construct($username,$password,$firstName,$lastName,$email){
        $this->setUsername($username);
        $this->setPassword($password);
        $this->setFirstName($firstName);
        $this->setLastName($lastName);
        $this->setEmail($email);
    }

    public function jsonSerialize(){
        $result = get_object_vars($this);
        return json_encode($result);
    }

    //setters
    public function setUserId($userId){
        $this->userId = $userId;
    }
    public function setUsername($username){
        $this->username = $username;
    }
    public function setPassword($password){
        $this->password = $password;
    }
    public function setFirstName($firstName){
        $this->firstName = $firstName;
    }
    public function setLastName($lastName){
        $this->lastName = $lastName;
    }
    public function setEmail($email){
        $this->email = $email;
    }
    public function setMemberSince($memberSince){
        $this->memberSince = $memberSince;
    }

    //getters
    public function getUserId(){
        return $this->userId;
    }
    public function getUsername(){
        return $this->username;
    }
    public function getPassword(){
        return $this->password;
    }
    public function getFirstName(){
        return $this->firstName;
    }
    public function getLastName(){
        return $this->lastName;
    }
    public function getEmail(){
        return $this->email;
    }
    public function getMemberSince(){
        return $this->memberSince;
    }

    public function generateMemberTimestamp(){
        $date = date("Y-m-d");
        $this->setMemberSince($date);
    }
}