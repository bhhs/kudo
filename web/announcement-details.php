<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 2/21/15
 * Time: 11:40 AM
 */

require "DbHelper.php";

$announceId = $_GET['id'];

$db = new DbHelper();
$announcement = $db->selectOneAnnouncement($announceId);

echo $announcement->jsonSerialize();