<?php
class CryptHelper{

    public function __construct(){ }

    //generate and verify code borrowed from: http://www.the-art-of-web.com/php/blowfish-crypt/
    static public function generate($input){
        $cost = 10;
        $salt = "";
        $salt_chars = array_merge(range('A','Z'), range('a','z'), range(0,9));
        for($i=0; $i < 22; $i++) {
            $salt .= $salt_chars[array_rand($salt_chars)];
        }
        return crypt($input, sprintf('$2a$%02d$', $cost) . $salt);
    }

    static public function verify($rawPassword,$hashAndSalt){
        //this is such a simple function it may be better to just use
        //the line below when needed, but keeping this method if
        //a future release needs the extra flexibility
        $result = password_verify($rawPassword, $hashAndSalt);
        return $result;
    }

}

?>