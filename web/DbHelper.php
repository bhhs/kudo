<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 12/18/14
 * Time: 10:28 PM
 */

//imports
require "Code.php";
require "Announcement.php";
require "User.php";
require "CryptHelper.php";
require "TokenGenerator.php";

class DbHelper {
    //database vars
    private $domain = "";
    private $database = "";
    private $username = "";
    private $password = "";

    //static table names
    static $TABLE_CODES = "codes";
    static $TABLE_ANNOUNCEMENTS = "announcements";
    static $TABLE_USERS = "users";
    static $TABLE_TOKENS = "tokens";

    private $db = null;

    public function __construct(){
        //get required variables outside of source control
        require_once("secrets.php");

        //update variables to be included in class scope
        $this->domain = $domain;
        $this->database = $database;
        $this->username = $username;
        $this->password = $password;

        //initiate the connection
        $this->connectToDatabase();
    }

    private function connectToDatabase(){
        $this->db = new mysqli($this->domain, $this->username, $this->password, $this->database);
    }

    public function insertCode(Code $codeObj){
        $TABLE_CODES = self::$TABLE_CODES;

        //get vars from code
        $code = $codeObj->getCode();
        $userId = $codeObj->getUserId();
        $name = $codeObj->getName();
        $description = $codeObj->getDescription();
        $photo = $codeObj->getPhoto();

        //escape characters
        $code = $this->db->real_escape_string($code);
        $name = $this->db->real_escape_string($name);
        $description = $this->db->real_escape_string($description);
        $photo = $this->db->real_escape_string($photo);

        $sql = "INSERT INTO $TABLE_CODES (Code, UserId, Name, Description, Photo) VALUES ( '$code', '$userId', '$name', '$description', '$photo' )";
        $this->db->query($sql) or die('Failed to insert Code object into database.');
    }

    public function insertAnnouncement(Announcement $announcementObj){
        $TABLE_ANNOUNCEMENTS = self::$TABLE_ANNOUNCEMENTS;

        //get vars from announcement
        $userId = $announcementObj->getUserId();
        $code = $announcementObj->getCode();
        $title = $announcementObj->getTitle();
        $content = $announcementObj->getContent();
        $effective = $announcementObj->getEffective();
        $expiry = $announcementObj->getExpiry();
        //$photoURL = $announcementObj->getPhotoURL();

        //escape characters
        $code = $this->db->real_escape_string($code);
        $title = $this->db->real_escape_string($title);
        $content = $this->db->real_escape_string($content);
        $effective = $this->db->real_escape_string($effective);
        $expiry = $this->db->real_escape_string($expiry);


        $sql = "INSERT INTO $TABLE_ANNOUNCEMENTS (UserId, Code, Title, Content, Effective, Expiry) VALUES ( '$userId', '$code', '$title', '$content', '$effective', '$expiry' )";
        $this->db->query($sql) or die('Failed to insert Announcement object into database.');
    }

    public function insertUser(User $userObj){
        $TABLE_USERS = self::$TABLE_USERS;

        //get vars from User
        $username = $userObj->getUsername();
        $password = $userObj->getPassword();
        $firstName = $userObj->getFirstName();
        $lastName = $userObj->getLastName();
        $email = $userObj->getEmail();

        //escape characters
        $username = $this->db->real_escape_string($username);
        $password = $this->db->real_escape_string($password);
        $firstName = $this->db->real_escape_string($firstName);
        $lastName = $this->db->real_escape_string($lastName);
        $email = $this->db->real_escape_string($email);

        $userObj->generateMemberTimestamp();
        $memberSince = $userObj->getMemberSince();

        if($this->userExists($username) == false) {
            $sql = "INSERT INTO $TABLE_USERS (Username, Password, FirstName, LastName, Email, MemberSince) VALUES ( '$username', '$password', '$firstName', '$lastName', '$email', '$memberSince' )";
            $this->db->query($sql) or die('Failed to insert User object into database.');
        }else{
            echo "Username already in use";
        }
    }

    public function selectOneCode($codeStr){
        //returns a code object if successful

        //escape codeStr
        $codeStr = $this->db->real_escape_string($codeStr);

        $TABLE_CODES = self::$TABLE_CODES;

        $sql = "SELECT * FROM $TABLE_CODES WHERE Code = '$codeStr' ";
        $result = $this->db->query($sql) or die('Failed to process SELECT Code query.');

        if ($result->num_rows == 1) {
            // output data of each row
            $row = $result->fetch_assoc();
            $codeObj = new Code($row['Code'],$row['UserId'],$row['Name'],$row['Description']);
            $codeObj->setId($row['Id']);
            $codeObj->setPhoto($row['Photo']);

            return $codeObj;

        } else if($result->num_rows == 0) {
            //echo "No results found for $codeStr";
        }else{
            //echo "Oops, something went wrong! More than one code was found matching this query.";
        }
    }

    public function selectManagedCodes($userId){
        $TABLE_CODES = self::$TABLE_CODES;

        $codesList = [];

        $sql = "SELECT * FROM $TABLE_CODES WHERE UserId = '$userId' ";
        $result = $this->db->query($sql) or die('Failed to process SELECT Codes query.');

        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                //fill all possible values of the announce object
                $codeObj = new Code($row['Code'],$row['UserId'],$row['Name'],$row['Description']);

                //append initialized announce object to the announcementsList
                $codesList[] = $codeObj->jsonSerialize();
            }
            return $codesList;
        }
        else if($result->num_rows == 0) {
            //echo "No results found for $userId";
        }else{
            //echo "Oops, something went wrong! More than one code was found matching this query.";
        }
    }

    public function selectAnnouncements($codesArray){
        //returns an array with all announcement objects matching the query
        $TABLE_ANNOUNCEMENTS = self::$TABLE_ANNOUNCEMENTS;

        $announcementsList = [];

        //process code string for sql
        //print_r($codesArray);
        $codesStr = "";

        $i = 0;
        foreach ($codesArray as $value) {
            if ($i > 0) {
                $codesStr = "$codesStr OR Code = '$value' ";
                $i++;
            }else{
                $codesStr = "Code = '$value' ";
                $i++;
            }
        }

        $sql = "SELECT * FROM $TABLE_ANNOUNCEMENTS WHERE $codesStr";
        //echo "<b>$sql</b><br>";
        $result = $this->db->query($sql) or die('Failed to process SELECT Announcements query.');

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                //fill all possible values of the announce object
                $announce = new Announcement($row['UserId'],$row['Code'],$row['Title']);
                $announce->setId($row['Id']);
                $announce->setContent($row['Content']);
                $announce->setEffective($row['Effective']);
                $announce->setExpiry($row['Expiry']);

                $codeObj = $this->selectOneCode($announce->getCode());
                $announce->setPhotoURL($codeObj->getPhoto());

                //append initialized announce object to the announcementsList
                $announcementsList[] = $announce->jsonSerialize();
            }
            return $announcementsList;

        } else {
            //echo "0 results";
        }
    }

    public function selectOneAnnouncement($announceId){
        $TABLE_ANNOUNCEMENTS = self::$TABLE_ANNOUNCEMENTS;

        $sql = "SELECT * FROM $TABLE_ANNOUNCEMENTS WHERE Id = '$announceId' ";
        $result = $this->db->query($sql) or die('Failed to process SELECT Announcements query.');

        $row = $result->fetch_assoc();
        $announce = new Announcement($row['UserId'],$row['Code'],$row['Title']);
        $announce->setId($row['Id']);
        $announce->setContent($row['Content']);
        $announce->setEffective($row['Effective']);
        $announce->setExpiry($row['Expiry']);
        $announce->setPhotoURL($row['Photo']);

        $codeObj = $this->selectOneCode($announce->getCode());
        $announce->setPhotoURL($codeObj->getPhoto());

        return $announce;
    }
    
    public function selectUser($username, $rawPassword)
    {
        $TABLE_USERS = self::$TABLE_USERS;

        $username = $this->db->real_escape_string($username);
        $rawPassword = $this->db->real_escape_string($rawPassword);

        $sql = "SELECT * FROM $TABLE_USERS WHERE Username = '$username' ";
        $result = $this->db->query($sql) or die('Failed to process SELECT Users query.');

        if ($result->num_rows == 1) {
            $row = $result->fetch_assoc();
            $user = new User($row['Username'], $row['Password'], $row['FirstName'], $row['LastName'], $row['Email']);
            $user->setUserId($row['Id']);
            $user->setMemberSince($row['MemberSince']);

            if( CryptHelper::verify($rawPassword,$user->getPassword()) ) {
                return $user;
            }else{
                return false;
            }

        }else{ return null; }
    }

    public function selectUserSudo($username)
    {
        $TABLE_USERS = self::$TABLE_USERS;

        $username = $this->db->real_escape_string($username);

        $sql = "SELECT * FROM $TABLE_USERS WHERE Username = '$username' ";
        $result = $this->db->query($sql) or die('Failed to process SELECT Users query.');

        if ($result->num_rows == 1) {
            $row = $result->fetch_assoc();
            $user = new User($row['Username'], "", $row['FirstName'], $row['LastName'], $row['Email']);
            $user->setUserId($row['Id']);
            $user->setMemberSince($row['MemberSince']);

            return $user;

        }else{ return null; }
    }

    public function userExists($username){
        $TABLE_USERS = self::$TABLE_USERS;

        $username = $this->db->real_escape_string($username);

        $sql = "SELECT * FROM $TABLE_USERS WHERE Username = '$username' ";
        $result = $this->db->query($sql) or die('Failed to process SELECT Users query.');

        if ($result->num_rows >= 1) {
            return true;
        }else{
            return false;
        }
    }

    public function generateCode($userId, $name, $description, $photo){
        $TABLE_ANNOUNCEMENTS = self::$TABLE_CODES;

        //escape chars
        $name = $this->db->real_escape_string($name);
        $description = $this->db->real_escape_string($description);
        $photo = $this->db->real_escape_string($photo);

        //retrieve last code used
        $sql = "SELECT Code FROM $TABLE_ANNOUNCEMENTS ORDER BY Code DESC LIMIT 1 ";
        $result = $this->db->query($sql) or die('Failed to find last existing code.');

        $row = $result->fetch_assoc();
        $lastCode = $row['Code'];

        //increment last used code to generate new, unique one
        $codeStr = ++$lastCode;

        //insert new code into DB
        $code = new Code($codeStr, $userId, $name, $description);
        $code->setPhoto($photo);
        $this->insertCode($code);

        return $code;
    }

    public function registerToken($username){
        $TABLE_TOKENS = self::$TABLE_TOKENS;

        $username = $this->db->real_escape_string($username);

        $sql = "DELETE FROM $TABLE_TOKENS WHERE Username = '$username' ";
        $this->db->query($sql) or die('Failed to remove old token from database.');

        $token = TokenGenerator::generate();

        $sql = "INSERT INTO $TABLE_TOKENS (Username, Token) VALUES ( '$username', '$token' )";
        $this->db->query($sql) or die('Failed to insert User object into database.');

        return $token;
    }

    public function verifyToken($username,$token){
        $TABLE_TOKENS = self::$TABLE_TOKENS;

        $username = $this->db->real_escape_string($username);

        $sql = "SELECT * FROM $TABLE_TOKENS WHERE Username = '$username' AND Token = '$token' ";
        $result = $this->db->query($sql) or die('Failed to find tokens.');

        if ($result->num_rows == 1) {
            return $this->registerToken($username);
        }

        return null;
    }

    public function usernameToUserId($username){
        $TABLE_USERS = self::$TABLE_USERS;

        $username = $this->db->real_escape_string($username);

        $sql = "SELECT Id FROM $TABLE_USERS WHERE Username = '$username' ";
        $result = $this->db->query($sql) or die('Failed to convert username to Id');

        $row = $result->fetch_assoc();
        return $row['Id'];
    }

}