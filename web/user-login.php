<?php
/**
 * Created by PhpStorm.
 * User: cybered
 * Date: 2/26/15
 * Time: 7:27 PM
 */

require "DbHelper.php";

//get vars from POST
$username = $_POST['user'];
$password = $_POST['password'];

function attemptLogin($username,$password){
    $db = new DbHelper();
    if ($db->userExists($username)) {
        $verifiedUser = $db->selectUser($username, $password);
        if ($verifiedUser != false) {
            return $db->registerToken($verifiedUser->getUsername());
        }
    } else {
        return null;
    }
}

echo (attemptLogin($username,$password));