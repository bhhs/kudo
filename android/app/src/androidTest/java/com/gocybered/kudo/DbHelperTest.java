package com.gocybered.kudo;

import android.test.AndroidTestCase;

import java.util.List;

public class DbHelperTest extends AndroidTestCase {
    DbHelper db;

    public void setUp() throws Exception {
        super.setUp();

        db = new DbHelper(getContext());
    }

    public void testSingleCode(){
        db.resetDatabase();

        db.addCode("TEST","ZZZZ");

        List<Code> codeList = db.getCodes();
        assertTrue(codeList.size() == 1);

        Code codeObj = codeList.get(0);
        assertTrue(codeObj.getName().equals("TEST"));
        assertTrue(codeObj.getCode().equals("ZZZZ"));
    }

    public void testMultipleCodes(){
        db.resetDatabase();

        db.addCode("TEST1","AAAA");
        db.addCode("TEST2","BBBB");
        db.addCode("TEST3","CCCC");
        db.addCode("TEST4","DDDD");

        List<Code> codeList = db.getCodes();
        assertTrue(codeList.size() == 4);
    }

    public void testUserAuth(){
        db.resetDatabase();

        String username = "testUser";
        String token = "07cc4d6982a45d39daa0ab1bd7e932c8";
        db.addToken(username, token);

        assertTrue(db.getToken().getUsername().equals(username));
        assertTrue(db.getToken().getToken().equals(token));

        String token2 = "e9f4723b58c67af6fc7cc7a0d3ceb155";
        db.addToken(username,token2);

        //ensure our first token is truly gone
        assertFalse(db.getToken().getToken().equals(token));
    }

}