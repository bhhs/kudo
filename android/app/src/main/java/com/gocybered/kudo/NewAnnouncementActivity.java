package com.gocybered.kudo;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class NewAnnouncementActivity extends KudoActivity
        implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener,
        NetworkInterface{

    private class DateAndTime{
        public int year = -1;
        public int month = -1;
        public int day = -1;

        public int hour = -1;
        public int minute = -1;

        public boolean hasAllValues(){
            if (year != -1 && month != -1 && day != -1 && hour != -1 && minute != -1){
                return true;
            }
            return false;
        }
    }

    private static final String TAG = "NewAnnouncementAct";

    private int action = 0;
    private static final int SET_DATE_START = 1;
    private static final int SET_DATE_END = 2;
    private static final int SET_TIME_START = 3;
    private static final int SET_TIME_END = 4;
    private static final int ONE_WEEK = 604800000;
    private static final int ONE_DAY = 86400000;

    private DateAndTime startCalendar = new DateAndTime();
    private DateAndTime endCalendar = new DateAndTime();

    private UserAuthenticator auth;
    private Code code;

    SimpleDateFormat dateFormat;
    SimpleDateFormat timeFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_announcement);

        //switch to show feed button in AB
        super.toggleManageAction(KudoActivity.SHOW_FEED_ACTION);

        Intent intent = getIntent();
        code = (Code) intent.getSerializableExtra(ManagementActivity.EXTRA_CODE_ID);

        TextView codeNameTV = (TextView) findViewById(R.id.codeNameTextView);
        codeNameTV.setText(code.getName());

        dateFormat = new SimpleDateFormat(getResources().getString(R.string.date_format));
        timeFormat = new SimpleDateFormat(getResources().getString(R.string.time_format));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.new_announcement, menu);

        auth = UserAuthenticator.findToken(this);

        return true;
    }

    private void launchManageActivity(){
        Toast.makeText(this, getString(R.string.success), Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this,ManagementActivity.class);
        startActivity(intent);
    }

    public void btnSubmit(View view){
        if(startCalendar.hasAllValues() && endCalendar.hasAllValues() && checkTitle()) {
            Announcement announcement = getAnnouncement();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("user", auth.getUsername()));
            nameValuePairs.add(new BasicNameValuePair("token", auth.spendToken()));
            nameValuePairs.add(new BasicNameValuePair("code", code.getCode()));
            nameValuePairs.add(new BasicNameValuePair("title", announcement.getTitle()));
            nameValuePairs.add(new BasicNameValuePair("description", announcement.getDescription()));
            nameValuePairs.add(new BasicNameValuePair("start", announcement.getStartEpoch().toString()));
            nameValuePairs.add(new BasicNameValuePair("end", announcement.getEndEpoch().toString()));

            new PostData(this,this,nameValuePairs).execute(Network.URL_NEW_ANNOUNCEMENT);
        }

    }

    private Announcement getAnnouncement() {
        //get ETs' values
        EditText titleET = (EditText) findViewById(R.id.eventNameEditText);
        EditText descriptionET = (EditText) findViewById(R.id.eventDescriptionEditText);
        String title = titleET.getText().toString();
        String description = descriptionET.getText().toString();

        //get start/end time values
        GregorianCalendar startGregorian =
                new GregorianCalendar(startCalendar.year,startCalendar.month,startCalendar.day,startCalendar.hour,startCalendar.minute);
        GregorianCalendar endGregorian =
                new GregorianCalendar(endCalendar.year,endCalendar.month,endCalendar.day,endCalendar.hour,endCalendar.minute);

        //add it all to an announcement
        Announcement announcement = new Announcement();
        announcement.setTitle(title);
        announcement.setDescription(description);
        announcement.setStartDate(startGregorian);
        announcement.setEndDate(endGregorian);

        return announcement;
    }

    private boolean checkTitle(){
        EditText titleET = (EditText) findViewById(R.id.eventNameEditText);
        String title = titleET.getText().toString();

        if(!title.isEmpty()){
            return true;
        }else {
            Toast.makeText(this,"Please enter a title.", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public void openDatePicker(View view){
        switch (view.getId()){
            case R.id.startDateButton:
                action = SET_DATE_START;
                break;
            case R.id.endDateButton:
                action = SET_DATE_END;
                break;
        }

        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(this, this, year, month, day);
        DatePicker datePicker= dialog.getDatePicker();
        datePicker.setMinDate(System.currentTimeMillis() - ONE_DAY);
        dialog.show();
    }

    public void openTimePicker(View view){
        switch (view.getId()){
            case R.id.startTimeButton:
                action = SET_TIME_START;
                break;
            case R.id.endTimeButton:
                action = SET_TIME_END;
                break;
        }

        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        new TimePickerDialog(this, this, hour, minute,
                DateFormat.is24HourFormat(this)).show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Button btn = null;
        if(action == SET_DATE_START) {
            btn = (Button) findViewById(R.id.startDateButton);
            startCalendar.year = year;
            startCalendar.month = monthOfYear;
            startCalendar.day = dayOfMonth;
        }else if(action == SET_DATE_END){
            btn = (Button) findViewById(R.id.endDateButton);
            endCalendar.year = year;
            endCalendar.month = monthOfYear;
            endCalendar.day = dayOfMonth;
        }

        //Parse date into HR String
        GregorianCalendar gc = new GregorianCalendar(year,monthOfYear,dayOfMonth);
        dateFormat.setCalendar(gc);
        String date = dateFormat.format(gc.getTime());

        btn.setText(date);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Button btn = null;
        if(action == SET_TIME_START) {
            btn = (Button) findViewById(R.id.startTimeButton);
            startCalendar.hour = hourOfDay;
            startCalendar.minute = minute;
        }else if(action == SET_TIME_END){
            btn = (Button) findViewById(R.id.endTimeButton);
            endCalendar.hour = hourOfDay;
            endCalendar.minute = minute;
        }

        //Parse date into HR String
        GregorianCalendar gc = new GregorianCalendar(2000,00,0,hourOfDay,minute);
        timeFormat.setCalendar(gc);
        String time = timeFormat.format(gc.getTime());

        btn.setText(time);
    }

    public void getData(String result) {
        try{
            JSONArray array = new JSONArray(result);
            for (int i = 0; i < array.length(); i++){
                JSONObject row = null;
                row = array.getJSONObject(i);

                if(row.has("token")){
                    Log.d(TAG, "Got new token!");
                    DbHelper db = new DbHelper(this);
                    db.addToken(auth.getUsername(),row.getString("token"));
                }
            }
            launchManageActivity();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "Received invalid JSON");
        } catch (NullPointerException e){
            e.printStackTrace();
            Log.e(TAG, "Failed to retrieve managed codes from server");
        }
    }
}
