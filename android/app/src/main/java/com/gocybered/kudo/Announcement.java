package com.gocybered.kudo;

import android.text.format.Time;

import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by cybered on 2/28/15.
 */
public class Announcement implements Serializable {
    private int id;
    private Code code;
    private String title = "";
    private String description = "";
    private GregorianCalendar startDate;
    private GregorianCalendar endDate;

    public Announcement(){ }

    public Long getStartEpoch(){
        return startDate.getTimeInMillis() / 1000;
    }
    public Long getEndEpoch(){
        return endDate.getTimeInMillis() / 1000;
    }

    //generic setter/getter

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Code getCode() {
        return code;
    }

    public void setCode(Code code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GregorianCalendar getStartDate() {
        return startDate;
    }

    public void setStartDate(GregorianCalendar startDate) {
        this.startDate = startDate;
    }

    public GregorianCalendar getEndDate() {
        return endDate;
    }

    public void setEndDate(GregorianCalendar endDate) {
        this.endDate = endDate;
    }
}
