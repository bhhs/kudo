package com.gocybered.kudo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


public class KudoActivity extends Activity {

//    This activity should be extended by any activity that uses the actionbar. 
//    This will prevent us from including the same menu icon on each activity.

    public static final int SHOW_FEED_ACTION = 1;
    public static final int SHOW_MANAGE_ACTION = 0;
    private int state = SHOW_MANAGE_ACTION;
    private boolean hideWatchAction = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.include, menu);

        if(state == SHOW_FEED_ACTION){
            menu.findItem(R.id.action_manage).setVisible(false);
            menu.findItem(R.id.action_feed).setVisible(true);
        }else if(state == SHOW_MANAGE_ACTION){
            menu.findItem(R.id.action_manage).setVisible(true);
            menu.findItem(R.id.action_feed).setVisible(false);
        }

        if(hideWatchAction){
            menu.findItem(R.id.action_watch_code).setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_watch_code) {
			Intent intent = new Intent(this, WatchCodeActivity.class);
			startActivity(intent);
			return true;
		}else if(id == R.id.action_manage){
            if(UserAuthenticator.hasToken(this)){
                Intent intent = new Intent(this, ManagementActivity.class);
                startActivity(intent);
            }else {
                Intent intent = new Intent(this, SignInActivity.class);
                startActivity(intent);
            }
            return true;
        } else if(id == R.id.action_feed){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            return true;
        /* } CURRENTLY ONLY USED FOR TESTING
        else if (id == R.id.action_settings) {
            DbHelper db = new DbHelper(this);
            db.resetDatabase();
            ImageStorage.deleteAllCached();
            Toast.makeText(this, "DB Reset!", Toast.LENGTH_SHORT).show();
            return true; */
        } else if (id == R.id.action_faqs){
            Intent intent = new Intent(this, HelpActivity.class);
            startActivity(intent);
        }
        //activity specific
        else if(id == R.id.action_create_code){
            Intent intent = new Intent(this, CreateCodeActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void toggleManageAction(int state){
        this.state = state;
        invalidateOptionsMenu();
    }

    public void hideWatchAction(){
        this.hideWatchAction = true;
        invalidateOptionsMenu();
    }

}
