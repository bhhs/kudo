package com.gocybered.kudo;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLHandshakeException;

/**
 * Created by cybered on 2/27/15.
 */
public class PostData extends AsyncTask<String, Integer, String> {
    private static final String TAG = "PostData";
    private static final String SSL_ERROR="SSL_ERROR";

    private Context context;
    private NetworkInterface activity = null;
    private List<NameValuePair> nameValuePairs;
    private ProgressDialog progressDialog;
    private boolean isConnected = false;

    public PostData(Context context, NetworkInterface activity, List<NameValuePair> nameValuePairs) {
        this.context = context;
        this.activity = activity;
        this.nameValuePairs = nameValuePairs;
    }

    @Override
    protected void onPreExecute() {
        try {
            progressDialog = ProgressDialog.show(context, "", "Loading", true);
            isConnected = NetworkManager.isConnected(context);
        } catch (final Throwable th) {
            Log.e(TAG,"Error onPreExecute()");
        }
    }

    @Override
    protected String doInBackground(String... strings) {
        HttpsURLConnection urlConnection;
        String responseString = "";

        if(isConnected) {
            try {
                URL url = new URL(Network.DOMAIN + strings[0]);

                //properly encode data into URL entity object
                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(nameValuePairs);

                //instantiate our SSL helper to accept and verify custom CA cert
                SslHelper secure = new SslHelper(context);

                //start the connection
                urlConnection = (HttpsURLConnection) url.openConnection();

                //use our custom SSL context from SslHelper above
                urlConnection.setSSLSocketFactory(secure.getSocketFactory());

                //prepare to send POST
                urlConnection.setUseCaches(false);
                urlConnection.setDoOutput(true);    //we are sending output to server
                urlConnection.setDoInput(true);     //we expect a response

                //create output stream and flush data
                urlConnection.setRequestMethod("POST");
                OutputStream post = urlConnection.getOutputStream();
                entity.writeTo(post);
                post.flush();

                //close OS
                post.close();

                int responseCode = urlConnection.getResponseCode();

                if (responseCode == HttpStatus.SC_OK) {
                    responseString = readStream(urlConnection.getInputStream());
                    Log.v(TAG, responseString);
                } else {
                    Log.v(TAG, "Response code:" + responseCode);
                }

                return responseString;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch(SSLHandshakeException e){
                Log.e(TAG, "Error with SSL handshake!");
                e.printStackTrace();
                return SSL_ERROR; // Temporary fix for more advanced error handling
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "Error sending request. Invalid character in string?");
            }
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        //do something
        super.onProgressUpdate(progress);
    }

    /*
     * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
     */
    @Override
    protected void onPostExecute(String result) {
        if(isConnected) {
            if (result == SSL_ERROR){
                Toast.makeText(context,context.getString(R.string.error_ssl_handshake),Toast.LENGTH_LONG).show();
            }
            activity.getData(result);
        }else{
            NetworkManager.displayError(context);
            Log.e(TAG,"No internet connection, not calling getData");
        }
        progressDialog.dismiss();
    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

}
