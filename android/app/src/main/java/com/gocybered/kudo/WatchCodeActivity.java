package com.gocybered.kudo;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class WatchCodeActivity extends KudoActivity implements NetworkInterface {

    public final static String CODE = "com.gocybered.kudo.CODE";
    private EditText editText;
    private String code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch_code);

        //switch to show feed button in AB
        super.hideWatchAction();

        editText = (EditText) findViewById(R.id.codeEditText);

        setTextListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.watch_code, menu);
        return true;
    }

    private void setTextListener() {
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    submitCode(v);
                    handled = true;
                }
                return handled;
            }
        });
    }

    public void submitCode(View view){
        //filter spaces from user-entered text
        code = editText.getText().toString();
        code = code.replaceAll("\\s+","");
        code = code.toUpperCase();
        //Check if code exists before we continue to the next activity
        new GetData(this,this).execute(Network.URL_CODE_EXISTS + code);
    }

    private void launchConfirmationActivity() {
        Intent intent = new Intent(this, WatchCodeConfirmationActivity.class);
        intent.putExtra(CODE, code);
        startActivity(intent);
    }

    public void getData(String result){
        if(result != null && result.equals("true")){
            launchConfirmationActivity();
        }else{
            Toast.makeText(this,getString(R.string.error_code_exist_not),Toast.LENGTH_LONG).show();
        }
    }
}
