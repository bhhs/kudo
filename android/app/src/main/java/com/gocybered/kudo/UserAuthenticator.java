package com.gocybered.kudo;

import android.content.Context;

/**
 * Created by cybered on 2/26/15.
 */
public class UserAuthenticator {
    Context context;
    DbHelper db;
    String username;
    String token;

    UserAuthenticator(Context context){
        this.context = context;
        db = new DbHelper(context);
    }

    UserAuthenticator(String username, String token, Context context){
        this.context = context;
        db = new DbHelper(context);
        setUsername(username);
        setToken(token);
    }

    static public UserAuthenticator findToken(Context context) {
        DbHelper db = new DbHelper(context);
        UserAuthenticator auth = db.getToken();
        return auth;
    }

    public boolean hasToken(){
        return hasToken(context);
    }

    static public boolean hasToken(Context context){
        DbHelper db = new DbHelper(context);
        UserAuthenticator auth = db.getToken();
        if (auth != null) {
            String token = auth.getToken();
            if (token != "" || token != null) {
                return true;
            }
        }
        return false;
    }

    public String getUsername() { return this.username; }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        if(token != null) {
            return token;
        }
        return "";
    }

    public String spendToken(){
        String token = this.getToken();
        this.invalidateToken();
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void invalidateToken(){
        db.invalidateToken();
    }

}
