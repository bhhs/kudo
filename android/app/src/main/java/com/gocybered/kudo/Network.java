package com.gocybered.kudo;

/**
 * Created by cybered on 2/27/15.
 */
public class Network {
    public static final String DOMAIN = "https://kudo.gocybered.com/";
    public static final String URL_USER_LOGIN = "user-login.php";
    public static final String URL_GENERATE_CODE = "generate-code.php";
    public static final String URL_ANNOUNCEMENT_DETAILS = "announcement-details.php?id=";
    public static final String URL_ANNOUNCEMENT_LOOKUP = "announcement-lookup.php?code=";
    public static final String URL_CODE_LOOKUP = "code-lookup.php?code=";
    public static final String URL_MANAGED_CODES_LOOKUP = "managed-code-lookup.php";
    public static final String URL_REGISTER_USER = "register-user.php";
    public static final String URL_NEW_ANNOUNCEMENT = "announcement-create.php";
    public static final String URL_CODE_EXISTS = "code-exists.php?code=";

    public static int TOKEN_LENGTH = 32;
}
