package com.gocybered.kudo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bryce on 12/14/14.
 */
public class DbHelper extends SQLiteOpenHelper {
    private static final String TAG = "DbHelper";
    private Context context;

    public static abstract class DbFollowedCodes implements BaseColumns {
        public static final String TABLE_NAME = "followed_codes";
        public static final String COLUMN_NAME_ID = "Id";
        public static final String COLUMN_NAME_NAME = "Name";
        public static final String COLUMN_NAME_CODE = "Code";

        public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_NAME_ID + " INTEGER PRIMARY KEY," +
                COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                COLUMN_NAME_CODE + TEXT_TYPE +
                " )";
        private static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public static abstract class DbUserAuth implements BaseColumns {
        public static final String TABLE_NAME = "auth";
        public static final String COLUMN_NAME_ID = "Id";
        public static final String COLUMN_NAME_USER = "User";
        public static final String COLUMN_NAME_TOKEN = "Token";

        public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
        COLUMN_NAME_ID + " INTEGER PRIMARY KEY," +
        COLUMN_NAME_USER + TEXT_TYPE + COMMA_SEP +
        COLUMN_NAME_TOKEN + TEXT_TYPE +
                 ")";
        private static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Main.db";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DbFollowedCodes.CREATE_TABLE);
        db.execSQL(DbUserAuth.CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            /*This database is only a cache for online data, so its upgrade policy is
            to simply to discard the data and start over*/
        db.execSQL(DbFollowedCodes.SQL_DELETE_ENTRIES);
        db.execSQL(DbUserAuth.SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void resetDatabase(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(DbFollowedCodes.SQL_DELETE_ENTRIES);
        db.execSQL(DbUserAuth.SQL_DELETE_ENTRIES);
        db.execSQL(DbFollowedCodes.CREATE_TABLE);
        db.execSQL(DbUserAuth.CREATE_TABLE);
    }

    public void addCode(String name, String code){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DbFollowedCodes.COLUMN_NAME_NAME, name);
        values.put(DbFollowedCodes.COLUMN_NAME_CODE, code);
        getWritableDatabase().insert(DbFollowedCodes.TABLE_NAME, null, values);
        database.close();
    }

    public List<Code> getCodes(){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + DbFollowedCodes.TABLE_NAME, null);

        int Id = cursor.getColumnIndex(DbFollowedCodes.COLUMN_NAME_ID);
        int Name = cursor.getColumnIndex(DbFollowedCodes.COLUMN_NAME_NAME);
        int Code = cursor.getColumnIndex(DbFollowedCodes.COLUMN_NAME_CODE);

        List<Code> list = new ArrayList();

        int id;
        String name;
        String code;

        if (cursor != null && cursor.moveToFirst()) {

            id = cursor.getInt(Id);
            name = cursor.getString(Name);
            code = cursor.getString(Code);

            Code codeObject = new Code(name,code);
            codeObject.setId(id);
            list.add(codeObject);

            while(cursor.moveToNext()){
                id = cursor.getInt(Id);
                name = cursor.getString(Name);
                code = cursor.getString(Code);

                codeObject = new Code(name,code);
                codeObject.setId(id);

                list.add(codeObject);
            }
        }
        return list;
    }

    public boolean hasCodes(){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + DbFollowedCodes.TABLE_NAME, null);

        int Id = cursor.getColumnIndex(DbFollowedCodes.COLUMN_NAME_ID);
        int Name = cursor.getColumnIndex(DbFollowedCodes.COLUMN_NAME_NAME);
        int Code = cursor.getColumnIndex(DbFollowedCodes.COLUMN_NAME_CODE);

        List<Code> list = new ArrayList();

        int id;
        String name;
        String code;

        if (cursor != null && cursor.moveToFirst()) {
            return true;
        }
        return false;
    }

    public void addToken(String username, String token){
        SQLiteDatabase database = this.getWritableDatabase();
        //clear all old entries
        database.execSQL(DbUserAuth.SQL_DELETE_ENTRIES);
        //recreate the table
        database.execSQL(DbUserAuth.CREATE_TABLE);
        //add the values
        ContentValues values = new ContentValues();
        values.put(DbUserAuth.COLUMN_NAME_USER, username);
        values.put(DbUserAuth.COLUMN_NAME_TOKEN, token);
        getWritableDatabase().insert(DbUserAuth.TABLE_NAME, null, values);
        database.close();
    }
    public UserAuthenticator getToken(){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + DbUserAuth.TABLE_NAME, null);

        int User = cursor.getColumnIndex(DbUserAuth.COLUMN_NAME_USER);
        int Token = cursor.getColumnIndex(DbUserAuth.COLUMN_NAME_TOKEN);

        String username;
        String token;

        if (cursor != null && cursor.moveToFirst()) {

            try {
                username = cursor.getString(User);
                token = cursor.getString(Token);

                UserAuthenticator auth = new UserAuthenticator(username, token, context);
                return auth;
            }catch (CursorIndexOutOfBoundsException e){
                Log.e(TAG,"Cursor out of bounds, but != null");
            }
        }

        return null;
    }
    public void invalidateToken(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(DbUserAuth.SQL_DELETE_ENTRIES);
        db.execSQL(DbUserAuth.CREATE_TABLE);
    }

}
