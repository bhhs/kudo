package com.gocybered.kudo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import android.support.v4.widget.SwipeRefreshLayout;


public class MainActivity extends KudoActivity implements NetworkInterface {
    private static final String TAG = "InfoSnap.MainActivity";
    public final static String EXTRA_ANNOUNCEMENT = "com.gocybered.kudo.EXTRA_ANNOUNCEMENT";

	private SwipeRefreshLayout swipeRefreshLayout;
	private boolean showLoadingDialog = true;
	private boolean isRefreshing = false;
    private DbHelper db;
    private ListView listView;
    private List<Announcement> announceList = new ArrayList<Announcement>();

    private String url = Network.URL_ANNOUNCEMENT_LOOKUP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
        db = new DbHelper(this);
		showLoadingDialog = true;
    }

    @Override
    protected void onResume(){
        super.onResume();
        refresh();
    }

    private void refresh() {
        if(isRefreshing == false){
			//reset list to prevent duplication
	        announceList = new ArrayList<Announcement>();
        	if(db.hasCodes()) {
				isRefreshing = true;
				createListView();
        	    sendAnnouncementRequest();
				showLoadingDialog = false;
        	}else{
            	renderFirstRunView();
        	}
		}
    }

    public void btnFAQ(View view){
        Intent intent = new Intent(this,HelpActivity.class);
        startActivity(intent);
    }

    private void sendAnnouncementRequest() {

        StringBuilder sb = new StringBuilder();
        sb.append(url);
        List<Code> list = db.getCodes();
        for (Code code: list) {
            sb.append(code.getCode());
            sb.append(",");
        }

        new GetData(this,this,showLoadingDialog).execute(sb.toString());
    }

    public void renderFirstRunView(){
        setContentView(R.layout.activity_main_first_run);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.watch_code, menu);
        return true;
    }

	private void createListView(){
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
				@Override
				public void onRefresh() {
					//TODO: Stop refresh only after finished
					Log.d(TAG,"Stopping refresh");
					swipeRefreshLayout.setRefreshing(false);
					refresh();
				}
			});
	}
	
    private void populateListView(List<Announcement> values) {
        ListViewAdapter adapter = new ListViewAdapter(this,values);
        listView = (ListView)findViewById(R.id.list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                startDetailActivity(position);
            }
        });
    }

    public void getData(String jsonString){
        isRefreshing = false;
		
		JSONArray jArray = null;
        List<Announcement> values;
        try {
            jArray = new JSONArray(jsonString);

            values = getAnnouncementNames(jArray);
            if (values.size() > 0){
                populateListView(values);
            }else {
                Toast.makeText(this,getString(R.string.error_no_upcoming_announcements),Toast.LENGTH_LONG).show();
            }
        }catch(JSONException e){
            e.printStackTrace();
        }catch (NullPointerException e){
            Log.e(TAG, "Error retrieving data");
            e.printStackTrace();
        }
    }

    private void startDetailActivity(int position) {
        Announcement announcement = announceList.get(position);

        Intent intent = new Intent(this, DetailViewActivity.class);
        intent.putExtra(EXTRA_ANNOUNCEMENT,announcement);
        startActivity(intent);
    }

    public List<Announcement> getAnnouncementNames(JSONArray array) {
        try{
            for (int i = 0; i < array.length(); i++){
                JSONObject row = null;
                row = array.getJSONObject(i);

                String name = row.getString("title");
                String description = row.getString("content");
                int id = row.getInt("id");
                String photoUrl = row.getString("photoURL");
                String codeStr = row.getString("code");
                Long effective = row.getLong("effective");
                Long expiry = row.getLong("expiry");

                //multiple unix timestamp by 1000 -> Java wants milliseconds
                Date startDate = new Date(effective*1000);
                Date endDate = new Date(expiry*1000);

                GregorianCalendar startCalendar = new GregorianCalendar();
                startCalendar.setTime(startDate);

                GregorianCalendar endCalendar = new GregorianCalendar();
                endCalendar.setTime(endDate);

                Code code = new Code(codeStr);
                code.setPhotoUrl(photoUrl);

                Announcement announce = new Announcement();
                announce.setId(id);
                announce.setTitle(name);
                announce.setDescription(description);
                announce.setCode(code);
                announce.setStartDate(startCalendar);
                announce.setEndDate(endCalendar);

                announceList.add(announce);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            e.printStackTrace();
            Log.e(TAG, "Failed to retrieve announcement names from server");

            //TODO: Add toast/dialog on fail
        }

        return  announceList;
    }
}
