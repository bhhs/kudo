package com.gocybered.kudo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by cybered on 3/1/15.
 */
public class GetImage extends AsyncTask<Object, Object, Object> {
    private String requestUrl, imagename;
    private ImageView imageView;
    private Bitmap bitmap ;
    private FileOutputStream fos;

    public GetImage(String requestUrl, ImageView view, String _imagename_) {
        this.requestUrl = requestUrl;
        this.imageView = view;
        this.imagename = _imagename_ ;
    }

    @Override
    protected Object doInBackground(Object... objects) {
        try {
            URL url = new URL(requestUrl);
            URLConnection conn = url.openConnection();
            bitmap = BitmapFactory.decodeStream(conn.getInputStream());
        } catch (Exception ex) {
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        if(!ImageStorage.checkIfImageExists(imagename))
        {
            imageView.setImageBitmap(bitmap);
            ImageStorage.saveToSdCard(bitmap, imagename);
        }
    }
}