package com.gocybered.kudo;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DetailViewActivity extends KudoActivity {

    private static final String url = Network.URL_ANNOUNCEMENT_DETAILS;

    private Announcement announcement;
    private TextView codeText;
    private TextView contentText;
    private TextView startDateText;
    private TextView endDateText;
    private TextView titleText;
    private ImageView imageView;
    private String start;
    private String end;
    private GregorianCalendar startCalendar;
    private GregorianCalendar endCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_view);

        Intent intent = getIntent();
        announcement = (Announcement) intent.getSerializableExtra(MainActivity.EXTRA_ANNOUNCEMENT);

        codeText = (TextView) findViewById(R.id.codeTextView);
        contentText = (TextView) findViewById(R.id.contentTextView);
        startDateText = (TextView) findViewById(R.id.startDateText);
        endDateText = (TextView) findViewById(R.id.endDateText);
        titleText = (TextView) findViewById(R.id.titleTextView);
        imageView = (ImageView) findViewById(R.id.imageView);

        populateFields(announcement);
    }

    private void populateFields(Announcement announce) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(getResources().getString(R.string.date_format));
        SimpleDateFormat timeFormat = new SimpleDateFormat(getResources().getString(R.string.time_format));
        startCalendar = announce.getStartDate();
        endCalendar = announce.getEndDate();

        dateFormat.setCalendar(startCalendar);
        timeFormat.setCalendar(startCalendar);
        start = dateFormat.format(startCalendar.getTime()) + " at " + timeFormat.format(startCalendar.getTime());

        dateFormat.setCalendar(endCalendar);
        timeFormat.setCalendar(endCalendar);
        end = dateFormat.format(endCalendar.getTime()) + " at " + timeFormat.format(endCalendar.getTime());

        String imageName = announce.getCode().getCode();

        if (ImageStorage.checkIfImageExists(imageName)) {
            File file = ImageStorage.getImage("/" + imageName + ".jpg");
            String path = file.getAbsolutePath();
            if (path != null) {
                Bitmap b = BitmapFactory.decodeFile(path);
                imageView.setImageBitmap(b);
            }
        }

        //set text
        codeText.setText(announce.getCode().getCode());
        contentText.setText(announce.getDescription());
        titleText.setText(announce.getTitle());
        startDateText.setText(start);
        endDateText.setText(end);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.detail_view, menu);

        //Because calendar intents require ICS, we need to disable the button if running on a lower API
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion < Build.VERSION_CODES.ICE_CREAM_SANDWICH){
            MenuItem item = menu.findItem(R.id.action_calendar);
            item.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        super.onOptionsItemSelected(item);

        int id = item.getItemId();

        if(id == R.id.action_share){
            StringBuilder messageBody = new StringBuilder();
            messageBody.append(announcement.getTitle());
            messageBody.append("\n");
            messageBody.append(announcement.getDescription());
            messageBody.append("\n\n");
            messageBody.append(getString(R.string.start));
            messageBody.append(" ");
            messageBody.append(start);
            messageBody.append("\n");
            messageBody.append(getString(R.string.end));
            messageBody.append(" ");
            messageBody.append(end);
            messageBody.append("\n\n");
            messageBody.append(getString(R.string.shared_with));

            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType(HTTP.PLAIN_TEXT_TYPE); //"text/plain" MIME type
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, announcement.getTitle());
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, messageBody.toString());

            // Verify that the intent will resolve to an activity
            if (sharingIntent.resolveActivity(getPackageManager()) != null) {
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_header)));
            }

        }else if(id == R.id.action_calendar){
            addEvent(announcement.getTitle(),"",startCalendar,endCalendar);
        }

        return super.onOptionsItemSelected(item);
    }

    //This is safe because button will never be displayed if on a SDK lower than ICS
    //The button is disabled in onCreateOptionsMenu()
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public void addEvent(String title, String location, Calendar begin, Calendar end) {
        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.Events.TITLE, title)
                .putExtra(CalendarContract.Events.EVENT_LOCATION, location)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, begin.getTimeInMillis())
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, end.getTimeInMillis());
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

}
