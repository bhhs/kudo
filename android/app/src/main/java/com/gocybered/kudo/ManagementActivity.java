package com.gocybered.kudo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ManagementActivity extends KudoActivity implements NetworkInterface{

    private static final String TAG = "ManagementActivity";
    public final static String EXTRA_CODE_ID = "com.gocybered.kudo.EXTRA_CODE_ID";

    private UserAuthenticator auth;
    List<Code> codeList = new ArrayList<Code>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //switch to show feed button in AB
        super.toggleManageAction(KudoActivity.SHOW_FEED_ACTION);

        try {
            auth = UserAuthenticator.findToken(this);
            String username = auth.getUsername();
            String token = auth.spendToken();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("user", username));
            nameValuePairs.add(new BasicNameValuePair("token", token));
            new PostData(this,this,nameValuePairs).execute(Network.URL_MANAGED_CODES_LOOKUP);
        }catch (NullPointerException e){
            Toast.makeText(this,"Please sign in.",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this,SignInActivity.class);
            startActivity(intent);
        }

    }

    private void createListView(String[] values){
            // Get ListView object from xml
            final ListView listView = (ListView) findViewById(R.id.list);

            // Define a new Adapter
            // First parameter - Context
            // Second parameter - Layout for the row
            // Third parameter - ID of the TextView to which the data is written
            // Fourth - the Array of data

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, android.R.id.text1, values);


            // Assign adapter to ListView
            listView.setAdapter(adapter);

            // ListView Item Click Listener
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {

                    // ListView Clicked item index
                    int itemPosition = position;

                    // ListView Clicked item value
                    String itemValue = (String) listView.getItemAtPosition(position);

                    startCodeManagement(itemPosition);
                }

            });
    }

    private void startCodeManagement(int position) {
        Code code = codeList.get(position);

        //Eventually, we will support advanced editing features and will need to send the user
        //to ManagementDetailActivity -- for now just send them to create a new announcement.
        Intent intent = new Intent(this, NewAnnouncementActivity.class);
        intent.putExtra(EXTRA_CODE_ID,code);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_management, menu);
        return true;
    }

    public void getData(String jsonString){
        JSONArray jArray = null;

        try {
            jArray = new JSONArray(jsonString);

            String[] values = getCodeNames(jArray);
            if(codeList.size() > 0) {
                setContentView(R.layout.activity_management);
                createListView(values);
            }else{
                setContentView(R.layout.activity_management_none);
            }
        }catch(JSONException e){
            e.printStackTrace();
        }catch (NullPointerException e){
            Log.e(TAG, "Error retrieving data");
            e.printStackTrace();
        }
    }

    public String[] getCodeNames(JSONArray array) {
        List<String> list = new ArrayList<String>();
        try{
            for (int i = 0; i < array.length(); i++){
                JSONObject row = null;
                row = array.getJSONObject(i);

                if(row.has("code") && row.has("name")) {
                    String name = row.getString("name");
                    String codeStr = row.getString("code");
                    Code code = new Code(name,codeStr);
                    codeList.add(code);
                    list.add(name + " (" + codeStr + ")");
                }else if(row.has("token")){
                    Log.d(TAG,"Got new token!");
                    DbHelper db = new DbHelper(this);
                    db.addToken(auth.getUsername(),row.getString("token"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            e.printStackTrace();
            Log.e(TAG, "Failed to retrieve managed codes from server");
        }

        return  list.toArray(new String[list.size()]);
    }

    public void btnNewCode(View view){
        Intent intent = new Intent(this, CreateCodeActivity.class);
        startActivity(intent);
    }

}
