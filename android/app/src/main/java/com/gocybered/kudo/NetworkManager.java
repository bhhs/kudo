package com.gocybered.kudo;

import android.content.*;
import android.net.*;
import android.widget.*;

public class NetworkManager
{

    static public boolean isConnected(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) 
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        boolean isConnected = (networkInfo != null && networkInfo.isConnected());
		
		return isConnected;
    }

    static public boolean displayErrorIfNotConnected(Context context){
        boolean isConnected = isConnected(context);

        if(isConnected != true){
            displayError(context);
        }

        return isConnected;
    }

    static public void displayError(Context context){
        Toast.makeText(context,context.getString(R.string.error_connection),Toast.LENGTH_LONG).show();
    }

}
