package com.gocybered.kudo;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;


public class SignInActivity extends KudoActivity implements NetworkInterface {

    private String username;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        //switch to show feed button in AB
        super.toggleManageAction(KudoActivity.SHOW_FEED_ACTION);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.sign_in, menu);
        return true;
    }

    public void btnSubmit(View view){
        validateInput();
        attemptLogin(username, password);
    }

    public void btnCreateAccount(View view){
        Intent intent = new Intent(this, CreateAccountActivity.class);
        startActivity(intent);
    }

    private void validateInput(){
        EditText usernameEditText = (EditText) findViewById(R.id.usernameEditText);
        EditText passwordEditText = (EditText) findViewById(R.id.passwordEditText);

        //TODO: Actually validate something here

        username = usernameEditText.getText().toString();
        password = passwordEditText.getText().toString();
    }

    private void attemptLogin(String username, String password){
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair("user", username));
        nameValuePairs.add(new BasicNameValuePair("password", password));

        new PostData(this,this,nameValuePairs).execute(Network.URL_USER_LOGIN);
    }

    public void getData(String result){
        if (result.length() == Network.TOKEN_LENGTH) {
            DbHelper db = new DbHelper(this);
            db.addToken(username, result);

            Toast.makeText(this, "Welcome " + username + ".", Toast.LENGTH_LONG).show();

            Intent intent = new Intent(this, ManagementActivity.class);
            startActivity(intent);
        }else{
            Toast.makeText(this, "Invalid username or password.", Toast.LENGTH_LONG).show();
        }
    }

}
