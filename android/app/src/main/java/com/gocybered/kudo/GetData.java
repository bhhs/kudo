package com.gocybered.kudo;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLHandshakeException;

/**
 * Created by cybered on 2/1/15.
 */
public class GetData extends AsyncTask<String, Integer, String> {
    private static final String TAG = "GetData";
    private static final String SSL_ERROR="SSL_ERROR";

    private Context context;
    private NetworkInterface activity = null;
    private ProgressDialog progressDialog;
    private boolean isConnected = false;
	private boolean showProgress = true;

	public GetData(Context context, NetworkInterface activity){
        this.context = context;
        this.activity = activity;
    }
	
    public GetData(Context context, NetworkInterface activity, boolean showProgress){
        this.context = context;
        this.activity = activity;
		this.showProgress = showProgress;
    }

    @Override
    protected void onPreExecute() {
        try {
			if(showProgress){
            	progressDialog = ProgressDialog.show(context, "", "Loading", true);
			}
            isConnected = NetworkManager.isConnected(context);
        } catch (final Throwable th) {
            Log.e(TAG,"Error onPreExecute()");
        }
    }

    @Override
    protected String doInBackground(String... strings) {
        HttpsURLConnection urlConnection;
        String responseString = "";

        if(isConnected) {
            try {
                URL url = new URL(Network.DOMAIN + strings[0]);

                SslHelper secure = new SslHelper(context);

                urlConnection = (HttpsURLConnection) url.openConnection();
                urlConnection.setSSLSocketFactory(secure.getSocketFactory());

                int responseCode = urlConnection.getResponseCode();

                if(responseCode == HttpStatus.SC_OK){
                    responseString = readStream(urlConnection.getInputStream());
                    Log.v(TAG, responseString);
                }else{
                    Log.v(TAG, "Response code:"+ responseCode);
                }

                return responseString;

            }  catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch(SSLHandshakeException e){
                Log.e(TAG, "Error with SSL handshake!");
                e.printStackTrace();
                return SSL_ERROR; // Temporary fix for more advanced error handling
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "Error sending request. Invalid character in string?");
            }
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        //do something
        super.onProgressUpdate(progress);
    }

    /*
     * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
     */
    @Override
    protected void onPostExecute(String result) {
        if(isConnected == true) {
            if (result == SSL_ERROR){
                Toast.makeText(context,context.getString(R.string.error_ssl_handshake),Toast.LENGTH_LONG).show();
            }
            activity.getData(result);
        }else{
            NetworkManager.displayError(context);
            Log.e(TAG,"No internet connection, not calling getData");
        }
		if(showProgress){
        	progressDialog.dismiss();
		}
    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

}
