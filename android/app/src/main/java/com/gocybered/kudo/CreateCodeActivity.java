package com.gocybered.kudo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CreateCodeActivity extends KudoActivity implements NetworkInterface {
    public final static String CODE = ManagementActivity.EXTRA_CODE_ID;
    private static final String TAG = "CreateCodeActivity";
    private static final String URL = Network.URL_GENERATE_CODE;
    private UserAuthenticator auth;
    private Code code;
    private Menu menu;

    private EditText nameEditText;
    private EditText descriptionEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_code);

        //switch to show feed button in AB
        super.toggleManageAction(KudoActivity.SHOW_FEED_ACTION);

        auth = UserAuthenticator.findToken(this);

        nameEditText = (EditText) findViewById(R.id.nameEditText);
        descriptionEditText = (EditText) findViewById(R.id.descriptionEditText);
    }

    public void btnSubmit (View view){
        requestCreateCode();
    }

    public void btnCreateAnnouncement(View view){
        Intent intent = new Intent(this, NewAnnouncementActivity.class);
        intent.putExtra(CODE, code);
        startActivity(intent);
    }

    private void requestCreateCode() {
        String name = nameEditText.getText().toString();
        String description = descriptionEditText.getText().toString();

        String username = auth.getUsername();
        String token = auth.spendToken();

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("user", username));
        nameValuePairs.add(new BasicNameValuePair("token", token));
        nameValuePairs.add(new BasicNameValuePair("name", name));
        nameValuePairs.add(new BasicNameValuePair("description", description));
        nameValuePairs.add(new BasicNameValuePair("photo",""));

        new PostData(this,this,nameValuePairs).execute(URL);
    }

    public void getData(String result) {
        try{
            JSONArray array = new JSONArray(result);
            for (int i = 0; i < array.length(); i++){
                JSONObject row = null;
                row = array.getJSONObject(i);

                if(row.has("code") && row.has("name")) {
                    String codeStr = row.getString("code");
                    String name = row.getString("name");
                    code = new Code(name,codeStr);
                }else if(row.has("token")){
                    Log.d(TAG,"Got new token!");
                    DbHelper db = new DbHelper(this);
                    db.addToken(auth.getUsername(),row.getString("token"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "Received invalid JSON");
        } catch (NullPointerException e){
            e.printStackTrace();
            Log.e(TAG, "Failed to retrieve managed codes from server");
        }

        if(code != null){
            setContentView(R.layout.activity_create_code_post);
            getMenuInflater().inflate(R.menu.create_code_post, menu);

            DbHelper db = new DbHelper(this);
            db.addCode(code.getName(),code.getCode());

            TextView codeTextView = (TextView) findViewById(R.id.codeTextView);
            codeTextView.setText(code.getCode());
        }else{
            Log.e(TAG,"CodeStr is null.");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);
        this.menu = menu;

		getMenuInflater().inflate(R.menu.create_code, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        super.onOptionsItemSelected(item);

        int id = item.getItemId();

        if(id == R.id.action_share){
            StringBuilder messageBody = new StringBuilder();
            messageBody.append(getString(R.string.invited_to_track));
            messageBody.append(" ");
            messageBody.append(code.getName());
            messageBody.append(getString(R.string.our_announcements_with_kudo));
            messageBody.append("\n");
            messageBody.append(getString(R.string.here_is_the_code));
            messageBody.append(" ");
            messageBody.append(code.getCode());
            messageBody.append(".\n\n");
            messageBody.append(getString(R.string.not_familiar));
            messageBody.append(" ");
            messageBody.append(getString(R.string.got_to_know));
            messageBody.append(" ");
            messageBody.append(getString(R.string.kudo));
            messageBody.append("\n\n");
            messageBody.append(getString(R.string.learn_more));

            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.share_code_subject));
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, messageBody.toString());

            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }

        return super.onOptionsItemSelected(item);
    }

}
