package com.gocybered.kudo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class WatchCodeConfirmationActivity extends KudoActivity implements NetworkInterface {

    private final static String TAG = "WatchCodeConfirmation";

    private TextView codeText = null;
    private TextView nameText = null;
    private TextView descriptionText = null;

    private String name = null;
    private String description = null;
    private String code = null;
    private String url = Network.URL_CODE_LOOKUP;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch_code_confirmation);

        Intent intent = getIntent();
        code = intent.getStringExtra(WatchCodeActivity.CODE);
        codeText = (TextView) findViewById(R.id.codeTextView);
        codeText.setText(getResources().getString(R.string.code) + " " + code);
        nameText = (TextView) findViewById(R.id.nameTextView);
        descriptionText = (TextView) findViewById(R.id.descriptionTextView);

        new GetData(this,this).execute(url+code);
    }

    public void getData(String jsonString){
        try{

            if(jsonString != null) {
                JSONObject jObject = new JSONObject(jsonString);
                populateFields(jObject);
            }else{
                Log.e(TAG, "Could not retrieve JSON object from server");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addCodeToDatabase(String name, String code){
        DbHelper db = new DbHelper(this);
        db.addCode(name,code);
    }

    private void populateFields(JSONObject jObject) throws JSONException {
        name = jObject.getString("name");
        description = jObject.getString("description");

        nameText.setText(getResources().getString(R.string.name) + " " + name);
        descriptionText.setText(getResources().getString(R.string.description) + " " + description);
    }

    public void confirm(View view){
        addCodeToDatabase(name,code);

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.watch_code_confirmation, menu);
        return true;
    }

}
