package com.gocybered.kudo;

import java.io.Serializable;

/**
 * Created by cybered on 2/24/15.
 */
public class Code implements Serializable{
    int id;
    String name;
    String code;
    String photoUrl;

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getCode() {
        return code;
    }
    public String getPhotoUrl() {
        return photoUrl;
    }

    public Code(String codeStr){
        this.code = codeStr;
    }

    public Code(String name, String code){
        this.name = name;
        this.code = code;
    }

    public void setPhotoUrl(String photoUrl){ this.photoUrl = photoUrl; }

    public void setId(int id){
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
