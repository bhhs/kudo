package com.gocybered.kudo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class CreateAccountActivity extends KudoActivity implements NetworkInterface {

    private static final String TAG = "CreateAccountActivity";
    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //switch to show feed button in AB
        super.toggleManageAction(KudoActivity.SHOW_FEED_ACTION);

        setContentView(R.layout.activity_create_account);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_create_account, menu);
        return true;
    }

    public void btnSubmit(View view){
        //TODO: Set ET Listeners to provide live feedback (eg. turning green if username is avail.)
        EditText usernameET = (EditText) findViewById(R.id.usernameEditText);
        EditText passwordET = (EditText) findViewById(R.id.passwordEditText);
        EditText confirmPasswordET = (EditText) findViewById(R.id.confirmPasswordEditText);
        EditText firstNameET = (EditText) findViewById(R.id.firstNameEditText);
        EditText lastNameET = (EditText) findViewById(R.id.lastNameEditText);
        EditText emailET = (EditText) findViewById(R.id.emailEditText);

        username = usernameET.getText().toString();
        String password = passwordET.getText().toString();
        String confirmPassword = confirmPasswordET.getText().toString();
        String firstName = firstNameET.getText().toString();
        String lastName = lastNameET.getText().toString();
        String email = emailET.getText().toString();

        if(password.equals(confirmPassword)){
            sendRequest(username,password,firstName,lastName,email);
        }else{
            Toast.makeText(this, getString(R.string.error_password_match), Toast.LENGTH_LONG).show();

            //wipe current password fields
            passwordET.setText("");
            confirmPasswordET.setText("");
        }
    }

    private void sendRequest(String username, String password, String firstName, String lastName,String email){
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("user", username));
        nameValuePairs.add(new BasicNameValuePair("password", password));
        nameValuePairs.add(new BasicNameValuePair("first", firstName));
        nameValuePairs.add(new BasicNameValuePair("last", lastName));
        nameValuePairs.add(new BasicNameValuePair("email", email));
        new PostData(this,this,nameValuePairs).execute(Network.URL_REGISTER_USER);
    }

    public void getData(String result){
        boolean success = false;
        try{
            JSONArray array = new JSONArray(result);
            for (int i = 0; i < array.length(); i++){
                JSONObject row = null;
                row = array.getJSONObject(i);

                if(row.has("token")){
                    success = true;
                    Log.d(TAG, "Got new token!");
                    DbHelper db = new DbHelper(this);
                    db.addToken(username,row.getString("token"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "Received invalid JSON");
        } catch (NullPointerException e){
            e.printStackTrace();
            Log.e(TAG, "Failed to retrieve managed codes from server");
        }

        if (success == true){
            Toast.makeText(this, "Welcome " + username + ".", Toast.LENGTH_LONG).show();

            Intent intent = new Intent(this,ManagementActivity.class);
            startActivity(intent);
        }else{
            Toast.makeText(this, getString(R.string.error_check_values), Toast.LENGTH_LONG).show();
        }

    }

}
