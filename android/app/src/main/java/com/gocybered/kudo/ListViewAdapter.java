package com.gocybered.kudo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.List;

/**
 * Created by cybered on 3/1/15.
 */
public class ListViewAdapter extends BaseAdapter {
    private List<Announcement> list;
    private Context context;

    public ListViewAdapter(Context context, List<Announcement> list){
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Announcement getItem(int arg0) {
        return list.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int arg0, View arg1, ViewGroup arg2) {

        if(arg1==null)
        {
            LayoutInflater inflater = LayoutInflater.from(context);
            arg1 = inflater.inflate(R.layout.list_item, arg2, false);
        }

        TextView titleTV = (TextView)arg1.findViewById(R.id.headingTextView);
        TextView descriptionTV = (TextView)arg1.findViewById(R.id.descriptionTextView);
        ImageView imageView = (ImageView) arg1.findViewById(R.id.imageView);

        //get announcement object
        Announcement announce = list.get(arg0);

        String imagename = announce.getCode().getCode();

        //if photoUrl is not empty
        if(!announce.getCode().getPhotoUrl().equals("")) {
            //if image already exists - just read it.
            if (ImageStorage.checkIfImageExists(imagename)) {
                File file = ImageStorage.getImage("/" + imagename + ".jpg");
                String path = file.getAbsolutePath();
                if (path != null) {
                    Bitmap b = BitmapFactory.decodeFile(path);
                    imageView.setImageBitmap(b);
                }
            } else {
                //if image does not exist, go get it.
                new GetImage(announce.getCode().getPhotoUrl(), imageView, imagename).execute();
            }
        }

        titleTV.setText(announce.getTitle());
        descriptionTV.setText(announce.getDescription());

        return arg1;
    }
}
